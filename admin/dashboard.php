
  <?php 
  session_start();

  if (@$_SESSION['admin_login'] != '') 
  {
     $email = $_SESSION['email'];
     define('TITLE', 'Admin Dashboard');
     define('PAGE', 'dashboard');
     define('MENU', 'menu');
     include('../headers/connection.php');
     include('sidebar.php'); 
   
  }
  else
  {
      echo "<script> location.href='login.php';</script>";
  }
    
     
     $sql = "SELECT count(*) as 'total' from requirement";
     $result = $conn->query($sql);
     $row = mysqli_fetch_row($result);
     $totalreq = $row[0];

     $sql = "SELECT count(*) as 'total' from dicussion";
     $result = $conn->query($sql);
     $row = mysqli_fetch_row($result);
     $totaldic = $row[0];

     $sql = "SELECT count(*) as 'total' from bugs";
     $result = $conn->query($sql);
     $row = mysqli_fetch_row($result);
     $totalbugs = $row[0];

     $sql = "SELECT count(*) as 'total' from changes";
     $result = $conn->query($sql);
     $row = mysqli_fetch_row($result);
     $totalchg = $row[0];

     $sql = "SELECT count(*) as 'total' from client";
     $result = $conn->query($sql);
     $row = mysqli_fetch_row($result);
     $totalclient = $row[0];

     $sql = "SELECT count(*) as 'total' from developer";
     $result = $conn->query($sql);
     $row = mysqli_fetch_row($result);
     $totaldev = $row[0];

     $sql = "SELECT count(*) as 'total' from user";
     $result = $conn->query($sql);
     $row = mysqli_fetch_row($result);
     $totaluser = $row[0];

     $sql = "SELECT count(*) as 'total' from admin";
     $result = $conn->query($sql);
     $row = mysqli_fetch_row($result);
     $totaladmin = $row[0];
   
      
  ?>



<div style="padding-top: 35px;">

<div class="row" style="text-decoration-color: white;">

<div class="col">
<div class="card text-white ttt mb-3 shadow" style="max-width: 20rem;">
<div class="card-header" style="font-size: larger;">Request's</div>
<div class="card-body">
<div class="row">
<div class="col">
<a href="detail_page/request.php" style="font-size: larger;">View Page</a>
</div>
<div class="col">
<a href="detail_page/request.php"><h4 class="card-title" align="right"><?php echo $totalreq; ?></h4></a>
</div>
</div>
</div>
</div>
</div>

<div class="col">
<div class="card text-white ttt mb-3 shadow" style="max-width: 20rem;">
<div class="card-header"> <font style="font-size: larger;">Dicussion's</font></div>
<div class="card-body">
<div class="row">
<div class="col">
<a href="detail_page/dicussion.php" style="font-size: larger;">View Page</a>
</div>
<div class="col">
<a href="detail_page/dicussion.php"><h4 class="card-title" align="right"><?php echo $totaldic; ?></h4></a>
</div>
</div>
</div>
</div>
</div>



<div class="col">
<div class="card text-white ttt mb-3 shadow" style="max-width: 20rem;">
<div class="card-header" style="font-size: larger;">Change's</div>
<div class="card-body">
<div class="row">
<div class="col">
<a href="changes.php" style="font-size: larger;" >View Page</a>
</div>
<div class="col">
<a href="changes.php"><h4 class="card-title" align="right"><?php echo $totalchg; ?></h4></a>
</div>
</div>
</div>
</div>
</div>



<div class="col">
<div class="card text-white ttt mb-3 shadow" style="max-width: 20rem;">
<div class="card-header" style="font-size: larger;">Bug's</div>
<div class="card-body">
<div class="row">
<div class="col">
<a href="bugs.php" style="font-size: larger;">View Page</a>
</div>
<div class="col">
<a href="bugs.php"><h4 class="card-title" align="right"><?php echo $totalbugs; ?></h4></a>
</div>
</div>
</div>
</div>
</div>






</div>

<div class="row" style="text-decoration-color: white;">

<div class="col">
<div class="card text-white ttt mb-3 shadow" style="max-width: 20rem;">
<div class="card-header"> <font color="white" style="font-size: larger;">Client</font></div>
<div class="card-body">
<div class="row">
<div class="col">
<a href="client.php" style="font-size: larger;">View Page</a>
</div>
<div class="col">
    
<a href="client.php"><h4 class="card-title" align="right"><?php echo $totalclient; ?></h4></a>
</div>
</div>
</div>
</div>
</div>

<div class="col">
<div class="card text-white ttt mb-3 shadow" style="max-width: 20rem;">
<div class="card-header" style="font-size: larger;">Developer</div>
<div class="card-body">
<div class="row">
<div class="col">
<a href="developer.php" style="font-size: larger;">View Page</a>
</div>
<div class="col">
<a href="developer.php"><h4 class="card-title" align="right"><?php echo $totaldev; ?></h4></a>
</div>
</div>
</div>
</div>
</div>


<div class="col">
<div class="card text-white ttt mb-3 shadow" style="max-width: 20rem;">
<div class="card-header"> <font style="font-size: larger;">Admin</font></div>
<div class="card-body">
<div class="row">
<div class="col">
<a href="admin.php" style="font-size: larger;">View Page</a>
</div>
<div class="col">
<a href="admin.php"><h4 class="card-title" align="right"><?php echo $totaladmin; ?></h4></a>
</div>
</div>
</div>
</div>
</div>

<div class="col">
<div class="card text-white ttt mb-3 shadow" style="max-width: 20rem;">
<div class="card-header" style="font-size: larger;">User</div>
<div class="card-body">
<div class="row">
<div class="col">
<a href="user.php" style="font-size: larger;">View Page</a>
</div>
<div class="col">
<a href="user.php"><h4 class="card-title" align="right"><?php echo $totaluser; ?></h4></a>
</div>
</div>
</div>
</div>
</div>



</div>


<div class="mx-5 mt-5 text-center">
<p class="ttt text-white p-2 shadow">List Of Requirement's</p>

<?php 

$sql = "SELECT subject, posted_date FROM requirement ORDER BY id ASC LIMIT 5";

$result = $conn->query($sql);
$count = 0; 

if ($result->num_rows > 0) 
  {

?> 

<table class="table table-stripd table-hover shadow-lg">
		<tr class="table-primary">
			<th>Sr.no</th>
			<th>Subject</th>
      <th>Posted Date</th>
      
    </tr> 

<?php
    $sno = $count+1;
		while ($row = $result->fetch_assoc()) 
          { 
            echo "<tr>";
		      	echo "<td>".$sno.".</td>";
			      echo "<td>".$row['subject']."</td>";
            echo "<td>".$row['posted_date']."</td>";
			      echo "</tr>";
      
            $sno++;
          }
echo "</table>";

    }

?>

  <ul class="pagination">
    <li class="page-item disabled">
      <a class="page-link" href="#">&laquo;</a>
    </li>
    <li class="page-item active">
      <a class="page-link" href="#">1</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">2</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">3</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">4</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">5</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">&raquo;</a>
    </li>
  </ul>




</div>
</div>

<?php include('../headers/footer.php'); ?>