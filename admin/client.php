 <?php 
     
     define('TITLE', 'CLIENT');
     define('PAGE', 'client');
     define('MENU', 'menu');
     include('../headers/connection.php');
     include('sidebar.php'); 
 ?>

  <div>


<div class="mx-5 mt-5 text-center">


<?php 

$sql = "SELECT * FROM client ORDER BY client_id ASC LIMIT 5";
$result = $conn->query($sql);
$count = 0; 

if ($result->num_rows > 0) 
  {

?> 

<table class="table table-stripd table-hover shadow-lg">
		<tr class="table-primary">
			<th>Sr.no</th>
			<th>Name</th>
			<th>E-mail</th>
            <th>Register Date</th>
            <th>Action</th>
      
    </tr> 

<?php
    $sno = $count+1;
		while ($row = $result->fetch_assoc()) 
          { 
          	echo "<tr>";
		    echo "<td>".$sno.".</td>";
			echo "<td>".$row['client_name']."</td>";
			echo "<td>".$row['client_email']."</td>";
            echo "<td>".$row['created_date']."</td>";
			echo "<td> <form action = '' method = 'post' class = 'd-inline'>
                        <input type='hidden' name='id' value = ".$row['client_id']."> 
                        <button type='submit' class='btn btn-link' style='color: blue;' 
                                   name='view' value='View'>
                        <i class='fa fa-eye'></i></button>
                    </form>
                    <form action = '' method = 'post' class = 'd-inline'>
                       <input type='hidden' name='id' value = ".$row['client_id'].">
                          <button type='submit' class='btn btn-link' style='color: puple;' 
                                  name='edit' value='Edit'>
                           <i class='fas fa-edit'></i></button>
                    </form>
                    <form action = '' method = 'post' class = 'd-inline'>
                       <input type='hidden' name='id' value = ".$row['client_id'].">
                          <button type='submit' class='btn btn-link' style='color: red;' 
                                  name='delete' value='Delete'>
                         <i class='fa fa-trash'></i></button>
                    </form></td>";
            echo "</tr>";
    
            $sno++;
          }
echo "</table>";

    }

?>

  <ul class="pagination">
    <li class="page-item disabled">
      <a class="page-link" href="#">&laquo;</a>
    </li>
    <li class="page-item active">
      <a class="page-link" href="#">1</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">2</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">3</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">4</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">5</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">&raquo;</a>
    </li>
  </ul>




</div>


 <div class= 'mx-5 mt-5 text-center'>
    <?php

    if (isset($_REQUEST['delete'])) 
          { 
               $sql = "DELETE FROM client where id = '".$_REQUEST['client_id']."'";

               if($conn->query($sql) == TRUE)
                 {

                   echo '<meta http-equiv="refresh" content="0;  
                               url=http://localhost/bit/resoftech/admin/client.php">';

                   echo '<div class="alert alert-success mt-2" roll="alert">
                    <font color="green"><h3>Request Successfully Deleted</h3></font></div>';
                 }
                else
                 {
                   echo '<div class="alert alert-danger mt-2" roll="alert">
                    <font color="red"><h3>Unable to Close.</h3></font></div>';
                 }
               
            }

    if (isset($_REQUEST['update'])) 
          {    

              $name = $_REQUEST['name'];
              $email = $_REQUEST['email'];
              $aboutyou = $_REQUEST['aboutyou'];
              $employeer = $_REQUEST['employeer'];
              $desig = $_REQUEST['desig'];
              $date = date("Y-m-d h:m:t");
             

              $sql = "UPDATE client SET client_name='".$name."', client_email='".$email."',
                      client_about='".$aboutyou."', client_company='".$employeer."',
                      client_desig='".$desig."', modified_date='".$date."' WHERE client_id = '".$_REQUEST['id']."'";
       
         if ($conn->query($sql) == TRUE) 
             {
                echo '<meta http-equiv="refresh" content="0;  
                               url=http://localhost/bit/resoftech/admin/client.php">';
             }
        else
             {
               echo '<div class="alert alert-success mt-2" roll="alert"><font color="Green"><h3>Error in Update </h3></font></div>';
             }    
      }

    if (isset($_REQUEST['edit'])) 
         { 
               $sql = "SELECT * FROM client where client_id = '".$_REQUEST['id']."'";
               $result = $conn->query($sql);
               if($result->num_rows > 0)
                 {
                 	$row = $result->fetch_assoc();
             ?>

 <div>
    <form method="post" class="shadow-lg p-4 mx-5" action="#" enctype="multipart/form-data">
      <h5 class="text-center"><strong>ADD CLIENT TO RESOFTECH</strong></h5>
       <hr>
      <div class="row">   
      <div class="form-group col-md-5">
      <label>Name</label>
      <input type="text" class="form-control" name="name" 
             value="<?php echo $row['client_name']; ?>">
      </div>

      <div class="form-group col-md-7">
      <label>Email</label>
      <input type="text" class="form-control" name="email" value="<?php echo $row['client_email']; ?>" >
      </div>

      <input type="hidden" class="form-control" name="id" value="<?php echo $row['client_id']; ?>" >
    

       </div>
    
      <div class="form-group">
      <label>About You</label>
      <textarea class="form-control" name="aboutyou" 
                rows="6" ><?php echo $row['client_about']; ?></textarea>
      </div>

      <div class="form-group">
      <label>Employeer</label>
      <input type="text" class="form-control" name="employeer" value="<?php echo $row['client_company']; ?>" >
      </div>



<div class="row ">

      <div class="form-group col">
      <label>Designation</label>
      <input type="text" class="form-control" name="desig" value="<?php echo $row['client_desig']; ?>">
      </div>
      

      <div class="form-group col">
      <label>Register date</label>
      <input type="text" class="form-control" name="info" value="<?php echo $row['created_date']; ?>"
             placeholder="<?php echo date("d-M-Y"); ?>" disabled>
      </div>


  </div>
      
   <div class="row">
      <div class="col">
       <button type="submit" name="update" 
               class="btn btn-outline-primary mt-3 font-weight-bold btn-block shadow-sm">Update</button>
      </div>
      <div class="col">  
             	<a href="client.php" 
                  class="btn btn-outline-dark mt-3 font-weight-bold btn-block shadow-sm">Reset</a>
      </div>
   </div>
 </form>

   

     </div>
            <?php
                 }
                else
                 {
                   echo '<div class="alert alert-danger mt-2" roll="alert">
                    <font color="red"><h3>Unable to open.</h3></font></div>';
                 }
               
        }

if (isset($_REQUEST['view'])) {
  
  $sql = "SELECT * FROM client WHERE client_id = {$_REQUEST['id']}";
  $result = $conn->query($sql);
  if ($result->num_rows == 1) 
            { 
               $row = $result->fetch_assoc();
               echo "<table class='table'>

<tbody>
    <tr class='table-active'>
      <th scope='row'>Client ID : </th>
      <td align='right'><b>".$row['client_id']."</b></td>
   </tr>
   
    <tr class='table-primary'>
      <th scope='row'>Name : </th>
      <td align='right'>".$row['client_name']."</td>
    </tr>

    <tr class='table-primary'>
      <th scope='row'>E-mail : </th>
      <td align='right'>".$row['client_email']."</td>
   </tr>

    <tr class='table-primary'>
      <th scope='row'>About Him : </th>
      <td align='right'>".$row['client_about']."</td>
    </tr>

    <tr class='table-primary'>
      <th scope='row'>Designation : </th>
      <td align='right'>".$row['client_desig']."</td>
    </tr>

    <tr class='table-primary'>
      <th scope='row'>Company : </th>
      <td align='right'>".$row['client_company']."</td>
    </tr>

  </tbody>
</table>";
echo '<a href="client.php"  class="btn btn-dark btn-lg btn-block" value="">RESET</a>';
}

else 
   {
   echo '<div class="alert alert-danger mt-3 mx-3" role="alert"><font color="Red"><h3>Request is still Pending...</h3></font></div>';
   }
}
?>
 
  </div>

   
</div>


<?php include('../headers/footer.php'); ?>