
<?php 
  session_start();

  if (@$_SESSION['admin_login'] != '') 
  {
    $email = $_SESSION['email'];
     define('TITLE', 'Assignment');
     define('PAGE', 'assignment');
     define('MENU', 'menu');
     include('../headers/connection.php');
     include('sidebar.php');  
   
  }
  else
  {
      echo "<script> location.href='login.php';</script>";
  }


?>

  <div>


<div class="mx-5 mt-5 text-center">
<p class="ttt text-white p-2 shadow">List Of assigned Task</p>

<?php 

$sql = "SELECT * FROM assigned ORDER BY id ASC LIMIT 6";

$result = $conn->query($sql);
$count = 0; 

if ($result->num_rows > 0) 
  {

?> 

<table class="table table-stripd table-hover shadow-lg">
    <tr class="table-primary">
      <th>Sr.no</th>
      <th>Category</th>
      <th>Subject</th>
      <th>Assign Date</th>
      <th>Employee</th>
      <th>Action</th>
    </tr> 

<?php
    $sno = $count+1;

    while ($row = $result->fetch_assoc()) 
          { 
            echo "<tr>";
            echo "<td>".$sno.".</td>";
            echo "<td>".$row['category']."</td>";
            echo "<td>".$row['subject']."</td>";
            echo "<td>".$row['assign_date']."</td>";
            echo "<td>".$row['assign_emp']."</td>";
            echo "<td> <form action = '' method = 'post' class = 'd-inline'>
                        <input type='hidden' name='id' value = ".$row['id']."> 
                        <button type='submit' class='btn btn-link' style='color: blue;' 
                                   name='view' value='View'>
                        <i class='fa fa-eye'></i></button>
                    </form>
                    <form action = '' method = 'post' class = 'd-inline'>
                       <input type='hidden' name='id' value = ".$row['id'].">
                          <button type='submit' class='btn btn-link' style='color: red;' 
                                  name='delete' value='Delete'>
                         <i class='fa fa-trash'></i></button>
                    </form></td>";
            echo "</tr>";
      
            $sno++;
          }
echo "</table>";


    }

else
    {
        echo '<div class="alert alert-danger mt-2" roll="alert">
                    <font color="red"><h3>No Data Available</h3></font></div>';
    }

 

?>

  </div>

  <div class= 'mx-5 mt-5 text-center'>
    <?php

    if (isset($_REQUEST['delete'])) 
          { 
               $sql = "DELETE FROM assigned where id = '".$_REQUEST['id']."'";

               if($conn->query($sql) == TRUE)
                 {

                   echo '<meta http-equiv="refresh" content="0;  
                               url=http://localhost/bit/resoftech/admin/assignment.php">';

                   echo '<div class="alert alert-success mt-2" roll="alert">
                    <font color="green"><h3>Request Successfully Deleted</h3></font></div>';
                 }
                else
                 {
                   echo '<div class="alert alert-danger mt-2" roll="alert">
                    <font color="red"><h3>Unable to Close.</h3></font></div>';
                 }
               
        }

if (isset($_REQUEST['id'])) {
  
  $sql = "SELECT * FROM assigned WHERE id = {$_REQUEST['id']}";
  $result = $conn->query($sql);
  if ($result->num_rows == 1) 
            {
               $row = $result->fetch_assoc();
               echo "<table class='table'>

<tbody>
    <tr class='table-active'>
      <th scope='row'>Request ID: </th>
      <td><b>".$row['id']."</b></td>
     
    </tr>
   
    <tr class='table-primary'>
      <th scope='row'>Subject: </th>
      <td>".$row['subject']."</td>
     
    </tr>

    <tr class='table-primary'>
      <th scope='row'>Discription: </th>
      <td>".$row['summary']."</td>
     
    </tr>

    <tr class='table-primary'>
      <th scope='row'>File Discription: </th>
      <td>".$row['file_desc']."</td>
     
    </tr>

    

    <tr class='table-primary'>
      <th scope='row'>Assigned To: </th>
      <td>".$row['assign_emp']."</td>
     
    </tr>

    <tr class='table-primary'>
      <th scope='row'>Assigned Date: </th>
      <td>".$row['assign_date']."</td>
     
    </tr>

 
   
  </tbody>
</table>";
echo '<a href="assignment.php"  class="btn btn-dark btn-lg btn-block" value="">Go Back</a>';
}

else 
   {
   echo '<div class="alert alert-danger mt-3 mx-3" role="alert"><font color="Red"><h3>Request is still Pending...</h3></font></div>';
   }
}
?>
 
  </div>

</div>




<?php include('../headers/footer.php'); ?>