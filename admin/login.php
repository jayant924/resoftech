
  <?php 
     session_start();
     define('TITLE', 'Admin Login');
     define('PAGE', 'adminlogin');
     include('../headers/header.php');
     include('../headers/connection.php'); 
  ?>

  
<?php

if (!isset($_SESSION['admin_login'])) {

if (isset($_REQUEST['email'])) {

$email = mysqli_real_escape_string($conn,trim($_REQUEST['email']));
$password = mysqli_real_escape_string($conn,trim($_REQUEST['password']));

$sql = "SELECT * FROM admin WHERE admin_email = '".$email."' AND admin_pass = '".$password."' LIMIT 1";
//echo $sql; die();


$result = $conn->query($sql);
if ($result->num_rows == 1) 
   {
    $row = $result->fetch_assoc();
    $name = $row['admin_name'];
    $_SESSION['admin_login'] = true;
    $_SESSION['email'] = $email;
    $_SESSION['name'] = $name;

     echo "<script> location.href='dashboard.php';</script>";
     exit;
   }
else 
   {
     $error = '<div class="alert alert-warning mt-2" role="alert"><font color="Red"><h3>Enter Valid Email and Password</h3></font></div>';
   }
  
 }

} 
 else
{
  echo "<script> location.href='dashboard.php';</script>";
}


?>

<div class="container" style="padding-top: 220px; padding-bottom: 200px;">
  <div class="row">
    <div class="col-md-3">
    </div>
    <div class="col-md-6">
 <form method="post" class="shadow-lg p-4" action="#">
 

      <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" class="form-control" name="email" value= "<?php ?>" placeholder="Enter Your Email">
      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
      </div>
    
      <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" name="password" placeholder="Enter Your Password">
      </div>
      
   
       <button type="submit" name='submit' 
               class="btn btn-outline-primary mt-3 font-weight-bold btn-block shadow-sm">Submit</button>
       <a href ="../admin/register.php" 
          class="btn btn-outline-secondary mt-3 font-weight-bold btn-block shadow-sm">Register here</a>
     

    </form>

   <?php 
           if (isset($error)) 
               {
                 echo $error; 
               }
    ?>
     

      </div>
      <div class="col-md-3">
      </div>
    </div>
</div>


 <?php include('../headers/footer.php'); ?>
 
