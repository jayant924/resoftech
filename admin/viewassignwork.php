<?php 
     
     define('TITLE', 'Assigned Work');
     define('PAGE', 'assigned_work');
     define('MENU', 'menu');
     include('../headers/connection.php');
     include('sidebar.php'); 

  ?>

  <div style="padding-top: 35px;">

     <?php 
        

if (isset($_REQUEST['id'])) {
	
	$sql = "SELECT * FROM assigned WHERE id = {$_REQUEST['id']}";
	$result = $conn->query($sql);
	if ($result->num_rows == 1) 
            {
               $row = $result->fetch_assoc();
               echo "<table class='table'>

<tbody>
    <tr class='table-active'>
      <th scope='row'>Request ID: </th>
      <td><b>".$row['id']."</b></td>
     
    </tr>
   
    <tr class='table-primary'>
      <th scope='row'>Subject: </th>
      <td>".$row['subject']."</td>
     
    </tr>

    <tr class='table-primary'>
      <th scope='row'>Discription: </th>
      <td>".$row['summary']."</td>
     
    </tr>

    <tr class='table-primary'>
      <th scope='row'>File Discription: </th>
      <td>".$row['file_desc']."</td>
     
    </tr>

    

    <tr class='table-primary'>
      <th scope='row'>Assigned To: </th>
      <td>".$row['assign_emp']."</td>
     
    </tr>

    <tr class='table-primary'>
      <th scope='row'>Assigned Date: </th>
      <td>".$row['assign_date']."</td>
     
    </tr>

 
   
  </tbody>
</table>";
}

else 
   {
	 echo '<div class="alert alert-danger mt-3 mx-3" role="alert"><font color="Red"><h3>Request is still Pending...</h3></font></div>';
   }
}
 

?>




  </div>

<?php include('../headers/footer.php'); ?>