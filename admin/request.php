    
<?php 
  session_start();
  if (@$_SESSION['admin_login'] != '') 
  {
     $email = $_SESSION['email'];
     define('TITLE', 'Request');
     define('PAGE', 'requests');
     define('MENU', 'menu');
     include('../headers/connection.php');
     include('sidebar.php'); 
  }
  else
  {
      echo "<script> location.href='login.php';</script>";
  }

     $sql = "SELECT * FROM requirement ORDER BY id ASC LIMIT 5";
     $result = $conn->query($sql);
     $count = 0; 

     if ($result->num_rows > 0) 
         {

?> 
                 <table class="table table-stripd table-hover shadow-lg">
                 <tr class="table-primary">
                 <th>Sr.no</th>
                 <th>Subject</th>
                 <th>Posted Date</th>
                 <th>Action</th>
                 </tr> 
<?php

                 $sno = $count+1;
                 while ($row = $result->fetch_assoc()) 
                       { 
                         echo "<tr>";
                         echo "<td>R".$sno.".</td>";
                         echo "<td>".$row['subject']."</td>";
                         echo "<td>".date("d-m-Y",strtotime($row['posted_date']))."</td>";
                         echo "<td> <form action = '' method = 'post' class = 'd-inline'>
                                    <input type='hidden' name='id' value = ".$row['id']."> 
                                    <button type='submit' class='btn btn-link' style='color: blue;' 
                                            name='view' value='View'>
                                   <i class='fa fa-eye'></i></button>
                                   
                                   
                                   <button type='submit' class='btn btn-link' style='color: green;' 
                                           name='update' value='Update'>
                                   <i class='fa fa-wrench'></i></button>

                                   <button type='submit' class='btn btn-link' style='color: red;' 
                                           name='delete' value='Delete'>
                                   <i class='fa fa-trash'></i></button>
                                   </form></td>";
                         echo "</tr>";
                         $sno++;
                        }
                         echo "</table>";

                         echo "<hr>";
                         
          }

     

if (isset($_REQUEST['view'])) {
 


// Post Detail page -------------------------------------------------------------------------

  $sql1 = "SELECT * FROM requirement where id = '".$_POST['id']."'";
  $result = $conn->query($sql1);

   if ($result->num_rows > 0) 
       { 
         while($row = $result->fetch_assoc())
              { 
                $record[] = $row;
              } 
            foreach($record as $detail)
                 {   
                    echo "<table class='table'>
                            <tbody>
                              <tr class='table-active'>
                                <th scope='row'>Client : </th>
                                 <td><b>".$detail['posted_by']."</b></td>
                              </tr>

                              <tr class='table-primary'>
                                <th scope='row'> Date: </th>
                                  <td>".date("d-m-Y",strtotime($detail['posted_date']))."</td>
                              </tr>
   
                              <tr class='table-primary'>
                               <th scope='row'>Discription: </th>
                                 <td>".$detail['summary']."</td>
                             </tr>
                          </tbody>";
                   echo '</table>';
         
           } 
        }


// Reply page --------------------------------------------------------------------------

$sql2 = "SELECT * FROM requirement_reply where req_id = '".$_POST['id']."' ORDER BY id ASC";
$result = $conn->query($sql2);

   if ($result->num_rows > 0) 
       { 
        
         while($row = $result->fetch_assoc())
              {   
               echo "<table class='table'>
                            <tbody>
                              <tr class='table-active'>
                                <th scope='row'>Developer : </th>
                                 <td><b>".$row['posted_by']."</b></td>
                              </tr>

                              <tr class='table-primary'>
                                <th scope='row'> Date: </th>
                                  <td>".date("d-m-Y",strtotime($row['posted_date']))."</td>
                              </tr>
   
                              <tr class='table-primary'>
                               <th scope='row'>Discription: </th>
                                 <td>".$row['summary']."</td>
                             </tr>
                          </tbody>";
                   echo '</table>';
         
           } 
        }

        echo "<table>";
                         echo "<tr>";
                         echo "<td> <form action = 'reply.php' method = 'post' class = 'd-inline'>
                                    <input type='hidden' name='id' value = ".$_POST['id']."> 
                                    <button type='submit' class='btn btn-link' style='color: blue;' 
                                            name='view' value='View'>
                                    <i class='fa fa-reply'></i> Reply </button>
                                    </form>
                                    <form action = '' method = 'post' class = 'd-inline'>
                                    <input type='hidden' name='id' value = ".$_POST['id'].">
                                    <button type='submit' class='btn btn-link' style='color: green;' 
                                            name='update' value='Update'>
                                    <i class='fa fa-undo'></i> Update </button>
                                    </form>
                                    <a href='request.php' class='btn btn-link' style='color: black;'><i class='fa fa-arrow-left'></i> Go Back </button></a></td>";
                         echo "</tr>";
                         echo "</table>";
    }


//  Delete Row page --------------------------------------------------------------------------------------------

              if (isset($_REQUEST['delete'])) 
                 {
                   $sql = "DELETE FROM requirement where id = '".$_REQUEST['id']."'";

                  if($conn->query($sql) == TRUE)
                     {
                       echo '<meta http-equiv="refresh" content="0;  
                               url=http://localhost/bit/resoftech/admin/request.php">';
                     }
                  else
                     {
                       echo '<div class="alert alert-danger mt-2" roll="alert">
                             <font color="red"><h3>Unable to Close.</h3></font></div>';
                     }
               
                 }
// update -----------------------------------------------------------------------------------------------------

              if (isset($_REQUEST['update'])) 
                 { 
                   
                   $sql = "SELECT * FROM requirement WHERE id = '".$_REQUEST['id']."'";

                   $result = $conn->query($sql);
                   if ($result->num_rows == 1) 
                      {
                        $row = $result->fetch_assoc();
                        $summary = $row['summary'];
                      }
?> 

<div> 
  <div class="col-sm-12">
   <form method="post" class="shadow-lg p-4 mx-5" action="#">
 

      <div class="form-group">
      <label for="exampleInputEmail1">ID : </label>
      <input type="text" class="form-control" name="id" value="<?php echo $row['id']; ?>" readonly >
      </div>
    
       <div class="form-group">
      <label>Summary</label>
      <textarea class="form-control" name="summary" 
                rows="6"><?php  echo @$row['summary']; ?></textarea>
      </div>
      
   
       <button type="submit" name='updatethis' 
               class="btn btn-outline-primary mt-3 font-weight-bold btn-block shadow-sm">Update</button>
       <a href ="request.php" 
          class="btn btn-outline-secondary mt-3 font-weight-bold btn-block shadow-sm">Back to Home</a> 
              
       <?php 
             if(isset($error))
               {
                echo $error;
               }
       ?>
    </form>
  </div>
</div>
                 
<?php
}

   if (isset($_REQUEST['updatethis'])) 
     {
        $summary = $_REQUEST['summary'];
        $sql = "UPDATE requirement SET summary = '".$summary."' WHERE id = '".$_REQUEST['id']."'";
           if ($conn->query($sql) == TRUE) 
               {
                 echo '<div class="alert alert-success mt-2" roll="alert"><font color="Green"><h3>Post updated  Sucessfully.</h3></font></div>';
               }
           else
               {
                 echo '<div class="alert alert-danger mt-2" roll="alert"><font color="red"><h3>Error in Update.</h3></font></div>';
               }
           
       }

 include('../headers/footer.php'); ?>